describe AnimalsController do
    describe '#show' do
        subject { described_class.new }
        let(:name) { 'John' }
        let(:kind) { 'aligator' }
        let(:aligator_object) do
            {'name' => 'John'}
        end

        before do
            expect(AnimalGateway).to receive_message_chain(:new, :get_animal).with(no_args).with(kind).and_return(aligator_object)
        end

        context 'aligator of name John' do
            it 'return proper name' do
                get :show, params: { kind: kind }
            end
        end
    end
end
