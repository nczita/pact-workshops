class AnimalsController < ApplicationController
  def show
    animal_kind = params[:kind]
    animal_name = GetAnimalService.new().get_animal(animal_kind)[:name]
  end
end
