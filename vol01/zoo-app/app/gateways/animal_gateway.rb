class AnimalGateway
    include HTTParty

    base_uri Rails.application.secrets.animal_service_url

    def get_animal(kind_of_animal)
        response = self.class.get("/api/animal/#{ kind_of_animal }", basic_auth: basic_auth)
        if response.success?
            Rails.logger.info("AnimalGateway#get_animal: Request success")
            response.parsed_response
        else
            Rails.logger.error("AnimalGateway#get_animal: Request failed, code: #{response.code}")
            raise "AnimalGateway#get_animal: Request failed, code: #{response.code}"
        end
    end

    private

    def basic_auth
        {
          username: Rails.application.secrets.animal_service_api_username,
          password: Rails.application.secrets.animal_service_api_password,
        }
    end
end
