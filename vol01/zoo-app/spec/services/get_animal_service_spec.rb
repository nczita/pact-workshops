describe GetAnimalService do
    describe '#get_animal' do
        subject { described_class.new }
        let(:body) do
            {'name' => 'John'}
        end

        before do
            stub_request(:get, 'http://localhost/api/animal/aligator').
                to_return(status: 200, body: body.to_json, headers: {'Content-Type' => 'application/json'})

        end

        context 'aligator of name John' do
            it 'return proper body' do
              expect(subject.get_animal('aligator')).to eq body
            end
        end
    end
end