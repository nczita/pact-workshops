Rails.application.routes.draw do
  get 'animals(/:kind)', to: 'animals#show'

  get 'welcome/index'

  root 'welcome#index'
end
